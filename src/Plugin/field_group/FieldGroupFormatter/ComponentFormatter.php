<?php

namespace Drupal\ui_patterns_field_group\Plugin\field_group\FieldGroupFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Render\Element;
use Drupal\field_group\FieldGroupFormatterBase;
use Drupal\field_group\Form\FieldGroupAddForm;
use Drupal\ui_patterns\ComponentPluginManager;
use Drupal\ui_patterns\Form\ComponentSettingsFormBuilderTrait;
use Drupal\ui_patterns\Resolver\ChainContextEntityResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'component' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "component_formatter",
 *   label = @Translation("Component"),
 *   description = @Translation("Wrap fields in a component."),
 *   supported_contexts = {
 *     "view",
 *   }
 * )
 */
class ComponentFormatter extends FieldGroupFormatterBase implements ContainerFactoryPluginInterface {

  use ComponentSettingsFormBuilderTrait;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\ui_patterns\ComponentPluginManager $componentPluginManager
   *   The UI Patterns component manager.
   * @param \Drupal\ui_patterns\Resolver\ChainContextEntityResolverInterface $chainContextEntityResolver
   *   The UI Patterns entity resolver.
   */
  public function __construct(
    array $configuration, $plugin_id, $plugin_definition,
    protected ComponentPluginManager $componentPluginManager,
    protected ChainContextEntityResolverInterface $chainContextEntityResolver,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $configuration['group'], $configuration['settings'], $configuration['label']);
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.sdc'),
      $container->get('ui_patterns.chain_context_entity_resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$element, $rendering_object) {
    $build = [];
    // Copy element's properties to the new render array.
    foreach (Element::properties($element) as $key) {
      $build[$key] = $element[$key];
    }

    $entity_type = $this->configuration['group']->entity_type;
    $entity = $rendering_object['#' . $entity_type];
    $source_contexts = [
      'entity' => $entity ? EntityContext::fromEntity($entity) : NULL,
      'entity_type' => new Context(ContextDefinition::create('string'), $entity_type),
      'bundle' => new Context(ContextDefinition::create('string'), $this->configuration['group']->bundle),
      'ui_patterns_field_group' => new Context(ContextDefinition::create('any'), (array) $this->configuration['group']),
      'ui_patterns_field_group:element' => new Context(ContextDefinition::create('any'), $element),
    ];
    $build['component'] = $this->buildComponentRenderable(source_contexts: $source_contexts);

    $element = $build;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    unset($form['id']);
    unset($form['classes']);

    if ($form_state->getFormObject() instanceof FieldGroupAddForm) {
      $form['message'] = [
        '#markup' => $this->t('<b>Attention:</b> Component selection and setting are done in the group edit form.'),
      ];
    }
    else {
      $injected_contexts = [
        'entity_type' => new Context(ContextDefinition::create('string'), $this->configuration['group']->entity_type),
        'bundle' => new Context(ContextDefinition::create('string'), $this->configuration['group']->bundle),
        'ui_patterns:form_state' => new Context(ContextDefinition::create('any'), $form_state),
        'ui_patterns_field_group' => new Context(ContextDefinition::create('any'), (array) $this->configuration['group']),
      ];
      if ($entity = $this->chainContextEntityResolver->guessEntity($injected_contexts)) {
        $injected_contexts['entity'] = EntityContext::fromEntity($entity);
      }
      $form['ui_patterns'] = $this->buildComponentsForm($form_state, $injected_contexts);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Get list of component by manager to insert label.
    $components = $this->componentPluginManager->getDefinitions();
    $options = [];
    $options['_empty'] = $this->t('Empty');
    foreach ($components as $component_id => $component) {
      $options[$component_id] = $component['name'];
    }
    $settings = $this->getSetting('ui_patterns');
    $summary["selected"] = $this->t('No component selected.')->render();
    if (!empty($settings['component_id'])) {

      $summary["selected"] = $this->t('Component ":component" selected.', [':component' => $options[$settings['component_id']] ?? ""])->render();
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultContextSettings($context) {
    return parent::defaultContextSettings($context) + self::getComponentFormDefault();
  }

  /**
   * {@inheritdoc}
   */
  public function getComponentSettings(): array {
    return $this->getSettings();
  }
}
