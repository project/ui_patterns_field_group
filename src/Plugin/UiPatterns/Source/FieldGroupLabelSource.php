<?php

declare(strict_types=1);

namespace Drupal\ui_patterns_field_group\Plugin\UiPatterns\Source;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\ui_patterns\Attribute\Source;
use Drupal\ui_patterns\SourcePluginBase;

/**
 * Plugin implementation of the source.
 */
#[Source(
  id: 'field_group_label',
  label: new TranslatableMarkup('Field group label'),
  description: new TranslatableMarkup('Label of the field group.'),
  prop_types: ['slot', 'string'],
  context_definitions: [
    'ui_patterns_field_group' => new ContextDefinition('any', label: new TranslatableMarkup('Field group configuration')),
  ]
)]
class FieldGroupLabelSource extends SourcePluginBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $isSlot = ($this->propDefinition["ui_patterns"]["type_definition"]->getPluginId() === "slot");
    $form['field_group'] = [
      '#type' => 'markup',
      '#markup' => '<em>' . $this->t('Field group label') . '</em>',
      '#access' => $isSlot,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropValue(): mixed {
    $isSlot = ($this->propDefinition["ui_patterns"]["type_definition"]->getPluginId() === "slot");
    $settings = $this->getContextValue('ui_patterns_field_group');
    $label = $settings['label'];
    if (empty($label) || !is_scalar($label)) {
      return $isSlot ? [] : "";
    }

    $filtered_label = $settings['format_settings']['label_as_html'] ?
      Markup::create(Xss::filterAdmin($label)) :
      Markup::create(Html::escape($label));
    return $isSlot ? ['#markup' => $filtered_label] : $filtered_label;
  }

}
