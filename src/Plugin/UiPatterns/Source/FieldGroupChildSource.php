<?php

declare(strict_types=1);

namespace Drupal\ui_patterns_field_group\Plugin\UiPatterns\Source;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\ui_patterns\Attribute\Source;
use Drupal\ui_patterns\SourcePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the source.
 */
#[Source(
  id: 'field_group_child',
  label: new TranslatableMarkup('Field group child'),
  description: new TranslatableMarkup('Child of the field group.'),
  prop_types: ['slot'],
  context_definitions: [
    'ui_patterns_field_group' => new ContextDefinition('any', label: new TranslatableMarkup('Field group configuration')),
    'ui_patterns_field_group:element' => new ContextDefinition('any', label: new TranslatableMarkup('Field group render array'), required: FALSE),
  ]
)]
class FieldGroupChildSource extends SourcePluginBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity_view_display storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $entityViewDisplayStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition,) {
    /** @var static $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityViewDisplayStorage = $container->get('entity_type.manager')->getStorage('entity_view_display');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $field_group_settings = $this->getContextValue('ui_patterns_field_group');
    $definitions = $this->entityFieldManager->getFieldDefinitions($field_group_settings['entity_type'], $field_group_settings['bundle']);
    $extra_fields = $this->entityFieldManager->getExtraFields($field_group_settings['entity_type'], $field_group_settings['bundle'])['display'];

    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $display */
    $display = $this->entityViewDisplayStorage->load(implode('.', [
      $field_group_settings['entity_type'],
      $field_group_settings['bundle'],
      $field_group_settings['mode'],
    ]));
    $field_groups = $display->getThirdPartySettings('field_group');

    $form['field_group_child'] = [
      '#type' => 'select',
      '#title' => $this->t('Field group child'),
      '#options' => [],
      '#default_value' => $this->getSetting('field_group_child'),
    ];
    foreach ($field_group_settings['children'] as $child_name) {

      $form['field_group_child']['#options'][$child_name] = match (TRUE) {
        !empty($definitions[$child_name]) => $definitions[$child_name]->getLabel(),
        !empty($extra_fields[$child_name]) => $extra_fields[$child_name]['label'],
        !empty($field_groups[$child_name]) => $field_groups[$child_name]['label'],
        default => $child_name,
      };
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropValue(): mixed {
    $element = $this->getContextValue('ui_patterns_field_group:element') ?? [];
    return $element[$this->settings['field_group_child']] ?? [];
  }

}
